<?php
# настройки
define ('DB_HOST', 'localhost');
define ('DB_LOGIN', 'root');
define ('DB_PASSWORD', '');
define ('DB_NAME', 'task2');
$connect = mysqli_connect(DB_HOST, DB_LOGIN, DB_PASSWORD) or die ("MySQL Error: " . mysqli_error($connect));
mysqli_query($connect, "set names utf8") or die ("<br>Invalid query: " . mysqli_error($connect));
mysqli_select_db($connect, DB_NAME) or die ("<br>Invalid query: " . mysqli_error($connect));

# массив ошибок
$error[0] = 'Неизвестный пользователь';
$error[1] = 'Включите cookie';
$error[2] = 'Доступ запрещен';
?>