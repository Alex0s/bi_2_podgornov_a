<?php
# подключаем конфиг
include_once 'config.php';

# проверка авторизации
if (isset($_COOKIE['id']) and isset($_COOKIE['hash'])) {
    $userdata = mysqli_fetch_assoc(mysqli_query($connect, "SELECT * FROM users WHERE users_id = '" . intval($_COOKIE['id']) . "' LIMIT 1"));

    if (($userdata['users_hash'] !== $_COOKIE['hash']) or ($userdata['users_id'] !== $_COOKIE['id'])) {
        setcookie('id', '', time() - 60 * 24 * 30 * 12, '/');
        setcookie('hash', '', time() - 60 * 24 * 30 * 12, '/');
        setcookie('errors', '1', time() + 60 * 24 * 30 * 12, '/');
        header('Location: ..\index.php');
        exit();
    } else {
        setcookie('login', $userdata['users_login'], time() + 60 * 24 * 30 * 12, '/');
        header('Location: ..\index.php');
        exit();
    }
} else {
    setcookie('errors', '2', time() + 60 * 24 * 30 * 12, '/');
    header('Location: ..\index.php');
    exit();
}
?>