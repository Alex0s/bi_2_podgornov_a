<?php
require_once('blocks/header.php');
?>
<?
if (isset($_POST['go_edit'])) {
    if (isset($_POST['title'])) {
        $title = $_POST['title'];
        if ($title == '') {
            unset($title);
        }
    }
    if (isset($_POST['description'])) {
        $description = $_POST['description'];
        if ($description == '') {
            unset($description);
        }
    }
    if (isset($_POST['selectionCategory'])) {
        $selectionCategory = $_POST['selectionCategory'];
        if ($selectionCategory == '') {
            unset($selectionCategory);
        }
    }
    if (isset($_POST['id'])) {
        $id = $_POST['id'];
    }
    if ($_POST['title'] != "" && $_POST['description'] != "" && $_POST['selectionCategory'] != "") {
        if (mysqli_query($connect, "UPDATE news SET
            news_title='" . $title . "', news_description='" . $description . "', cat_id='" . $selectionCategory . "' WHERE news_id='" . $id . "'")) {
            echo "<div class='bg-info'>Новость обновлена!</div>";
        } else {
            echo "<div class='bg-info'>Ошибка в редактировании новости<div>";
            $dar = mysqli_error($connect);
            echo $dar;
        }
    } else {
        echo "<div class='bg-info'><p>Заполните все поля</p></div>";
    }
} elseif (isset($_GET['id'])) {
    $id = $_GET['id'];
    $query = mysqli_query($connect, "SELECT * FROM news WHERE news_id='" . $id . "'");
    if ($query && mysqli_num_rows($query) > 0) {
        $data = mysqli_fetch_assoc($query);
        $title = $data["news_title"];
        $description = $data["news_description"];
        $id = $data["news_id"];
        $cat_id = $data["cat_id"]; ?>
        <div id="content">
            <form name="form1" method="post" action="">
                <h2>Изменение новости</h2>
                <p>
                    Введите название <br>
                    <input type="text" style="border:1px silver solid; width:160px;" value="<?php echo $title; ?>"
                           name="title" id="title">
                    <br>Введите описание <br>
                    <input type="text" style="border:1px silver solid; width:160px;" value="<?php echo $description; ?>"
                           name="description" id="description">
                    <input type="hidden" value="<?php echo $id; ?>" name="id" id="id">
                    <?php
                    $result = mysqli_query($connect, "SELECT * FROM `categories`");
                    if ($result) {
                        if (mysqli_num_rows($result) > 0) {
                            echo "<div class=\"form-group\">";
                            echo "<label for='selectionCategory'>Каегория:</label><br>";
                            echo "<select name='selectionCategory', id='selectionCategory'>";
                            while ($myrow = mysqli_fetch_array($result)) {
                                set_time_limit(0);
                                if ($myrow['cat_id'] == $cat_id) {
                                    echo "<option value=" . $myrow['cat_id'] . " selected>" . $myrow['cat_name'] . "</option>";
                                } else {
                                    echo "<option value=" . $myrow['cat_id'] . " >" . $myrow['cat_name'] . "</option>";
                                }
                            }
                            echo "</select></div>";
                        }
                    }
                    ?>
                </p>
                <input type="submit" class="buttons" name="go_edit" id="submit" value="Отправить">
            </form>
        </div>
    <?php } else {
        echo "<div class='bg-info'>Новости не существует<div>";
    }
    ?>
    <?php
} else {
    echo "<div class='bg-info'>Новость не выбрана<div>";
}
require_once('blocks/footer.php');
?>
