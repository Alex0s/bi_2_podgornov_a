﻿<?php
require_once('blocks/header.php');
?>
<?php
if (isset($_POST['submit'])) {

    $err = array();

    # проверям логин
    if (!preg_match("/^[a-zA-Z0-9]+$/", $_POST['login'])) {
        $err[] = "Логин может состоять только из букв английского алфавита и цифр";
    }

    if (strlen($_POST['login']) < 3 or strlen($_POST['login']) > 30) {
        $err[] = "Логин должен быть не меньше 3-х символов и не больше 30";
    }

    if (!preg_match("/^[a-zA-Z0-9]+$/", $_POST['password'])) {
        $err[] = "Пароль может состоять только из букв английского алфавита и цифр";
    }

    if (strlen($_POST['password']) < 3 or strlen($_POST['password']) > 30) {
        $err[] = "Пароль должен быть не меньше 3-х символов и не больше 30";
    }
    $login = htmlspecialchars ($_POST['login']);
    # проверяем, не сущестует ли пользователя с таким именем
    $query = mysqli_query($connect, "SELECT * FROM users WHERE users_login='" . $login . "'") or die ("<br>Invalid query: " . mysqli_error());
    if (mysqli_num_rows($query) != 0) {
        $err[] = "Пользователь с таким логином уже существует в базе данных";
    }


    # Если нет ошибок, то добавляем в БД нового пользователя
    if (count($err) == 0) {

        $login = $_POST['login'];

        # Убераем лишние пробелы и делаем двойное шифрование
        $password = md5(md5(trim($_POST['password'])));

        mysqli_query($connect, "INSERT INTO users SET users_login='" . $login . "', users_password='" . $password . "'");
        header("Location: index.php");
        exit();
    }
}
?>
    <div id="content">
        <div class="block2">
            <h2>Регистрация</h2>
            <form action="" method="post">
                <div class="form-group">
                    <label for="login">Ваш логин: </label>
                    <input type="text" class="form-control" id="login" name="login" placeholder="Логин">
                </div>
                <div class="form-group">
                    <label for="password">Ваш пароль</label>
                    <input type="password" class="form-control" name="password" id="password" placeholder="Пароль">
                </div>
                <button type="submit" name="submit" class="btn btn-default">Зарегистрироваться</button>
            </form>
        </div>
    </div>
<?php
if (isset($err)) {
    echo "<div class='bg-info'>При регистрации произошли следующие ошибки:</b><br>";
    foreach ($err AS $error) {
        print $error . "<br>";
    }
    echo "</div>";
}
?>
<?php
require_once('blocks/footer.php');
