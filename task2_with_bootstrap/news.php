<?php
require_once('blocks/header.php');
?>
<?php
$result = mysqli_query($connect, "SELECT * FROM `news`");

if (!$result) {
    echo "<div class='bg-info'>Запрос на выборку данных из базы не прошел. <br> <strong>Код ошибки:</strong></p>";
    exit(mysqli_error($connect));
    echo "</div>";
} ?>
    <div id="content">
        <a href="addnews.php">Добавить новость</a>
        <?php if (mysqli_num_rows($result) > 0) {
            $myrow = mysqli_fetch_array($result);
            ?>
    <h2 class="sub-header">Список новостей</h2>
    <div class="table-responsive">
        <table class="table table-striped">
            <thead>
            <tr>
                <th>Заголовок новости</th>
                <th>Описание новости</th>
                <th>Категория</th>
                <th>Действие</th>
            </tr>
            </thead>
            <tbody>
            <?php do {
                $cat_result = mysqli_query($connect, "SELECT cat_name FROM `categories` where cat_id='" . $myrow["cat_id"] . "'");
                if ($cat_result && mysqli_num_rows($cat_result) > 0){
                    $category = mysqli_fetch_assoc($cat_result);
                    $category_name = $category['cat_name'];
                }
                else{
                    $category_name = "Категории не существует";
                }
                $editlink = "editnews.php?id=" . $myrow['news_id'];
                $dellink = "delnews.php?id=" . $myrow['news_id'];
                ?>

                <tr>
                    <td><?php echo $myrow["news_title"]; ?></td>
                    <td><?php echo $myrow["news_description"]; ?></td>
                    <td><?php echo $category_name; ?></td>
                    <td><a href="<?php echo $editlink; ?>">Редактировать</a><br>
                    <a href="<?php echo $dellink; ?>">Удалить</a></td>
                </tr>


            <?php } while ($myrow = mysqli_fetch_array($result)); ?>

        <?php } ?>
            </tbody>
        </table>
    </div>
    </div>
<?php
require_once('blocks/footer.php');
?>