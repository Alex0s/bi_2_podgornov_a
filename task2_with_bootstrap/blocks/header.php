<?php require_once("authorization/config.php"); ?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <title>Мой сайт</title>

    <!-- Bootstrap core CSS -->
    <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css\dashboard.css" rel="stylesheet">
    <script src="bootstrap/js/bootstrap.min.js"></script>
</head>

<body>

<nav class="navbar navbar-inverse navbar-fixed-top">
    <div class="container-fluid">
        <div class="navbar-header">
            <a class="navbar-brand" href="#">Мой сайт</a>
        </div>
    </div>
</nav>

<div class="container-fluid">
    <div class="row">
        <div class="col-sm-3 col-md-2 sidebar">
            <ul class="nav nav-sidebar">
                <li><a href="index.php">Главная</a></li>
                <li><a href="news.php">Новости</a></li>
                <li><a href="categories.php">Категория</a></li>
            </ul>
            <?php
            if (isset($_COOKIE['login'])) {
                ?>
                <div>
                    <h2>Добро пожаловать, <?php echo $_COOKIE['login']; ?></h2>
                    <form action="index.php" method="post"><input type='submit' name='exit' value='Выйти'/></form>
                </div>
            <?php } else { ?>
            <form action="index.php" method="post">
                <div class="form-group">
                    <label for="login">Логин</label>
                    <input type="text" class="form-control" id="login" name="login" placeholder="Логин">
                </div>
                <div class="form-group">
                    <label for="password">Пароль</label>
                    <input type="password" class="form-control" id="password" name="password" placeholder="Пароль">
                </div>
                <button type="submit" name="submit" class="btn btn-default">Авторизоваться</button>
                <div class="form-group">
                    <p>Если вы не зарегистрированы в системе, <a href="registration.php">зарегистрируйтесь</a></p>
                </div>
            </form>
            <?php } ?>
        </div>
        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">