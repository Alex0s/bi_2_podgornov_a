﻿<?php
require_once('blocks/header.php');
?>
<?php
# Функция для генерации случайной строки
function generateCode($length = 6)
{
    $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPRQSTUVWXYZ0123456789";
    $code = "";
    $clen = strlen($chars) - 1;
    while (strlen($code) < $length) {
        $code .= $chars[mt_rand(0, $clen)];
    }
    return $code;
}

# Если есть куки с ошибкой то выводим их в переменную и удаляем куки
if (isset($_COOKIE['errors'])) {
    $errors = $_COOKIE['errors'];
    setcookie('errors', '', time() - 60 * 24 * 30 * 12, '/');
}

# Подключаем конфиг
include_once 'authorization\config.php';
if($_REQUEST['exit'])
{
    setcookie('id', '', time() - 60*60*24*30, '/');
    setcookie('login', '', time() - 60*60*24*30, '/');
    setcookie('hash', '', time() - 60*60*24*30, '/');
    header('Location: index.php'); exit();
}

if (isset($_POST['submit'])) {

    # Вытаскиваем из БД запись, у которой логин равняеться введенному
    $data = mysqli_fetch_assoc(mysqli_query($connect, "SELECT users_id, users_password FROM `users` WHERE `users_login`='" . mysqli_real_escape_string($connect, $_POST['login']) . "' LIMIT 1"));

    # Соавниваем пароли
    if ($data['users_password'] === md5(md5($_POST['password']))) {
        # Генерируем случайное число и шифруем его
        $hash = md5(generateCode(10));

        # Записываем в БД новый хеш авторизации и IP
        mysqli_query($connect, "UPDATE users SET users_hash='" . $hash . "' WHERE users_id='" . $data['users_id'] . "'") or die("MySQL Error: " . mysqli_error());

        # Ставим куки
        setcookie("id", $data['users_id'], time() + 60 * 60 * 24 * 30);
        setcookie("hash", $hash, time() + 60 * 60 * 24 * 30);

        # Переадресовываем браузер на страницу проверки нашего скрипта
        header("Location: authorization\check.php");
        exit();
    } else {
        echo "<div class='bg-info'>Вы ввели неправильный логин/пароль</div>";
    }
}
?>
<?php
if (isset($_COOKIE['login'])) {
    ?>
        <div>
            <h2>Вы успешно вошли!</h2>
        </div>
<?php } else { ?>
        <div>
            <h2>Главная страница</h2>
        </div>
<?php } ?>
<?php
# Проверяем наличие в куках номера ошибки
if (isset($errors)) {
    print '<h4>' . $error[$errors] . '</h4>';
}

?>
<?php
require_once('blocks/footer.php');
?>




