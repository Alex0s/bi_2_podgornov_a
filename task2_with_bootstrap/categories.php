<?php
require_once('blocks/header.php');
?>
<?php
$result = mysqli_query($connect, "SELECT * FROM `categories`");

if (!$result) {
    echo "<div class='bg-info'>Запрос на выборку данных из базы не прошел. <br> <strong>Код ошибки:</strong></p>";
    exit(mysqli_error($connect));
    echo "</div>";
} ?>
    <div id="content">
        <a href="addcategory.php">Добавить категорию</a>
        <?php if (mysqli_num_rows($result) > 0) {
        $myrow = mysqli_fetch_array($result);
        ?>
        <h2 class="sub-header">Список категорий</h2>
        <div class="table-responsive">
            <table class="table table-striped">
                <thead>
                <tr>
                    <th>Наименование категории</th>
                    <th>Действие</th>
                </tr>
                </thead>
                <tbody>
            <?php do {
                $editlink = "editcategory.php?id=" . $myrow['cat_id'];
                $dellink = "delcategory.php?id=" . $myrow['cat_id'];
                ?>

                <tr>
                    <td><?php echo $myrow["cat_name"]; ?></td>
                    <td><a href="<?php echo $editlink; ?>">Редактировать</a><br>
                        <a href="<?php echo $dellink; ?>">Удалить</a></td>
                </tr>


            <?php } while ($myrow = mysqli_fetch_array($result)); ?>

            <?php } ?>
            </tbody>
        </table>
        </div>
    </div>
<?php
require_once('blocks/footer.php');
?>